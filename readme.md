# Geo-localisation avec [LeafletJS](http://leafletjs.com), [Turf](http://turfjs.org/) et [GeoJSON](http://geojson.org) 

Vous pouvez utiliser les tutoriels sur le site de leafletjs pour vous aider, notamment celui sur GeoJson et le Quick-Start.
Utiliser Turfjs pour inverser les coordonnées des GeoJSON.
GeoJSON est exprimé au format lon,lat alors que Leaflet est au format lat,lon.


### TP1 

1. Avec LeafletJS  afficher une carte simple. Avec un fond OpenStreetMap.
2. Afficher la localisation de l’utilisateur en utilisant la fonction HTML5 de géolocalisation
3. Ajouter une popUp ouverte sur le marqueur de l’utilisateur.
4. Importer le fichier “table_1000.geojson” (disponible [ici](https://rtsinfo.carto.com/tables/table_1000/public/map)) en utilisant la méthode “L.geoJSON” 
    * Le fichier GeoJSON doit etre modifé pour lui ajouter un nom de variable comme suit:  `data={"type": "FeatureCollection", "features": [{"type":"Feature","geometry":`.
et afficher les marqueurs correspondant
5. Transformer l’importation du fichier pour utiliser la méthode “onEachFeature”
    * pour créer les marqueurs, 
    * leurs ajouter une popup affichant la propriété “adresse”
6. Modifier l’apparence des marqueurs


### TP2

1. Importer et afficher les points de collisions depuis le fichier :
    * collision-locations.geojson sur le site [OpenData NorthernIreland](https://www.opendatani.gov.uk/dataset/police-recorded-injury-road-traffic-collision-statistics-northern-ireland)
2. En utilisant le guide pdf associé (page 3 et 4) choisir un critère de différenciation (la gravité (a_type),  le nombre de véhicules (a_veh), … par exemple) 
    * et créer des marqueurs circulaire dont la taille (radius), la transparence ou la couleur varie en fonction de la valeur de la propriété.
    * _expl : change la couleur du marker en fonction de la valeur de la variable "Collision Severity"_
3. Filtrer les points à afficher en fonction du district en utilisant la fonction “Filter”


### TP2-2
4. Ajouter un menu déroulant pour choisir le district à afficher.

